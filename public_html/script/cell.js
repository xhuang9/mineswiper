var Cell = function(i, j, d){
    this.i = i;
    this.j = j;
    this.x = i*d;
    this.y = j*d;
    this.d = d;
    this.mine = false;
    this.reveal = false;
}

Cell.prototype.draw = function(){
    console.log("draw "+ this.i,this.j);
    context.beginPath();
    context.rect(this.x+1, this.y+1, this.d-2, this.d-2);
    context.fillStyle = "gray";  
    context.fill();
    context.closePath();
    if(this.reveal == true){
        if(this.mine === true){
            context.beginPath();
            context.arc(this.x + this.d/2 ,this.y + this.d/2 , this.d /4 ,0 ,2*Math.PI);
            context.fillStyle= "red";
            context.fill();
            context.strokeStyle= "black";
            context.stroke();
            context.closePath();
        }
        if(this.mine === false){
            this.neighbour();
            if(this.count !== 0)
            {
            context.strokeText(this.count, this.x + this.d/3, this.y + this.d /1.5);
            }
        }
    }
}

Cell.prototype.neighbour = function(){
    if(this.mine === true){
        this.count = -1;
        return;
    }
    if(this.mine === false){
        total = 0;
        for(x = this.i - 1; x <= this.i + 1; x++){
            for(y = this.j - 1; y <= this.j + 1; y++){
                if(x >= 0 && y >=0 && x < row && y < col){
                    if(grid[x][y].mine === true){
                       total ++;
                    }
                }
            }
        }
        this.count = total;
    }
}

Cell.prototype.contains = function(x,y){
    return (x > this.x && x < this.x + this.d && y > this.y && y < this.y + this.d );
}

//Cell.prototype.floodfill = function(){
//    this.neighbour();
//    this.reveal = true;
//    this.draw();
//    console.log("floodfill count is " + this.count);
//    console.log("reveal set true");
//    if (this.count == 0){
//        console.log("flood fill time");
//        this.groupreveal();
//    }
//    console.log("flood fill end");
//}

Cell.prototype.floodfill = function(){
    this.neighbour();
    this.reveal = true;
    if(this.count === 0){
        Array = this.floodfillArray();
        if(Array === null){
            console.log("null");
        }
        for(l = 0; l < Array.length; l++){
            console.log("x: " + Array[l]["x"] + ", y: " + Array[l]["y"] + ", count = " + grid[Array[l]["x"]][Array[l]["y"]].count + ", mine = " + grid[Array[l]["x"]][Array[l]["y"]].mine);
            if(grid[Array[l]["x"]][Array[l]["y"]].mine === false){
                grid[Array[l]["x"]][Array[l]["y"]].reveal = true;
                grid[Array[l]["x"]][Array[l]["y"]].draw();
            }
        }
    }
    this.draw();
}

Cell.prototype.floodfillArray = function(z){
    if(z===undefined){
        z = 0;
    }
    Flood=[];
    count = 0;
    for(gx = (this.i - 1); gx <= (this.i + 1); gx++){
        for(gy = (this.j - 1); gy <= (this.j + 1); gy++){
            if(gx >= 0 && gy >= 0 && gx < row && gy < col){
                console.log(this.i + " " + this.j);
                var neighbor = grid[gx][gy];
                Flood[count]= {x:gx, y:gy, z:z};
               if(neighbor.reveal === false && neighbor.mine === false){
                    neighbor.reveal = true;
                
                count++;
                //enter another layer of flood fill
                    if(neighbor.count === 0){
    //                    for(nx = (neighbor.i - 1); nx <= (neighbor.i + 1); nx++){
    //                        for(ny = (neighbor.j - 1); ny <= (neighbor.j + 1); ny++){
    //                            if(nx >= 0 && ny >= 0 && nx < row && ny < col){
    //                                if(lookupInArray(Flood, [nx, ny, z])){
    //                                    z++;
    //                                    console.log(neighbor.i + " " + neighbor.j);
    //                                    Flood[count]= {x:nx, y:ny, z:z};
    //                                    count++;
    //                                }
    //                            }
    //                        }
    //                    }
                        neighbor.floodfillArray(z);
                    }
                }
            }
        }
    }
    return Flood;
}


//Cell.prototype.groupreveal = function(){
//    for(gx = (this.i - 1); gx <= (this.i + 1); gx++){
//        for(gy = (this.j - 1); gy <= (this.j + 1); gy++){
//            console.log("groupreveal loop to " + gx, gy);
////            console.log("groupreveal is " + grid[gx][gy].reveal);
//            if(gx >= 0 && gy >= 0 && gx < row && gy < col){
//                var neighbor = grid[gx][gy];
//                if(neighbor.reveal == false && neighbor.mine == false){
//                    neighbor.floodfill();
//                }
//            }
//        }
//    }
//}

