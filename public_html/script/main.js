create2DArray = function(){
    var Arr = [];
    for(i = 0; i < col; i++){
        var A = [];
        for(j = 0; j< row; j++){
            A[j] = [i*w,j*w];
        }
        Arr[i] = A;
    }
    return Arr;
}

function make2DArray(cols, rows) {
  var arr = new Array(cols);
  for (var i = 0; i < arr.length; i++) {
    arr[i] = new Array(rows);
  }
  return arr;
}

function lookupInArray(array, element){
    for(i=0;i<=array.length;i++){
        if(array[i] === element){
            return true;
        }else{
            return false;
        }
    }    
}

drawGrid = function(){
    context.beginPath();
    for(i = 0; i < col; i++){
        for(j = 0; j< row; j++){
            context.moveTo(Arr[i][j][0], Arr[i][j][1]);
            context.lineTo(Arr[i][j][0] + w, Arr[i][j][1]);
            context.lineTo(Arr[i][j][0] + w, Arr[i][j][1] + w);
            context.lineTo(Arr[i][j][0], Arr[i][j][1] + w);
            context.lineTo(Arr[i][j][0], Arr[i][j][1]);
        }
    }
    context.lineWidth = "1";
    strokeStyle="black";
    context.stroke();
}
setupcell = function(){
for(i = 0; i < col; i++){
        for(j = 0; j< row; j++){
            //draw individual cell
            grid[i][j] = new Cell(i,j, w);
        }
    }
    return cells;
}
function getMousePos(canvas, event){
    var rect = canvas.getBoundingClientRect();
    return {
        x:event.clientX - rect.left,
        y:event.clientY - rect.top
    };
}

function drawallshouldshow(){
    for(i = 0; i < col; i++){
        for(j = 0; j< row; j++){
            //draw individual cell
            grid[i][j].draw();
        }
    }
}

gameover = function(){
      for(i = 0; i < col; i++){
        for(j = 0; j< row; j++){
            //draw individual cell
            grid[i][j].reveal = true;
            grid[i][j].draw();
        }
    }
}

mouseClick = function(event){
    var mousePos = getMousePos(canvas, event);
    for(i = 0; i < col; i++){
        for(j = 0; j< row; j++){
            if(grid[i][j].contains(mousePos.x,mousePos.y)){
                if(grid[i][j].reveal === false){
                    grid[i][j].floodfill(grid[i][j].i,grid[i][j].j);
                }
                if(grid[i][j].mine === true){
                    gameover();
                }
            }
        }
    }
}

//setup canvas
var col = 30; //col number
var row = 30; //row number
var w = 20; //width
var height = col * w + 1;
var width = row * w + 1;
var BackGroundColor = "white"; //background color
var totalMines = 100; //number of mines to be set
var canvas = document.getElementById("myCanvas");
canvas.width = width;
canvas.height = height;
var context = canvas.getContext("2d");
context.clearRect(0, 0, canvas.width, canvas.height);
context.fillStyle = BackGroundColor;
context.fillRect(0, 0, canvas.width, canvas.height);
var Arr = create2DArray();
var grid = make2DArray(col, row);
drawGrid();
var cells = setupcell();
//pick random number of Mine
var mineMap = [];
for(n = 0; n < totalMines; n ++){
    var j = Math.floor(Math.random() * col);
    var i = Math.floor(Math.random() * row);
    grid[i][j].mine = true;
    mineMap.push([i,j]);
    console.log(i,j);
}
console.log(mineMap);
canvas.addEventListener("click", mouseClick, false);







